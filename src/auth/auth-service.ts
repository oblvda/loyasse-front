import { User } from "@/entities";
import axios from "axios";



export  async function postRegister(user:User) {
    const response = await axios.post<{user:User, token:string}>('/api/user', user);
    return response.data;
}

export  async function postLogin(email:string, password:string) {
    const response = await axios.post<{token:string}>('/api/login', {email,password});
    return response.data;
}
