import { destroyCookie, parseCookies, setCookie } from "nookies";
import { createContext, useEffect, useState } from "react";



interface AuthProps {
    token: string | null;
    setToken: (value: string | null) => void;
}

export const AuthContext = createContext<AuthProps>({} as AuthProps);

export function AuthContextProvider({ children }: any) {

    const [token, setToken] = useState<string | null>(null);

    useEffect(() => {
        const cookies = parseCookies(null, 'token');

        setToken(cookies.token);

    }, []);

    function handleToken(value:string|null) {
        if(value) {
            setCookie(null, 'token', value);
        } else {
            destroyCookie(null, 'token');
        }
        setToken(value);
    }

    return <AuthContext.Provider value={{token, setToken:handleToken}}>
        {children}
    </AuthContext.Provider>
}