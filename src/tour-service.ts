import axios from "axios";
import { Tour } from "./entities";

export async function fetchAllTours() {
    const response = await axios.get<Tour[]>('http://localhost:8000/api/tour');
    return response.data;
}

export async function postTour(tour:Tour) {
    const response = await axios.post<Tour>('http://localhost:8000/api/tour', tour);
    return response.data;
}