import banner from '../assets/bannerindex.jpg'


export default function BannerIndex() {

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="justify-content-center no-gutters px-0">
                        <img src={banner.src} className="vw-100 vh-100 object-fit-cover"></img>
                        <h1 className="centered text-white">Le cimetière de Loyasse</h1>
                    </div>
                </div>
            </div>
        </>
    )
}