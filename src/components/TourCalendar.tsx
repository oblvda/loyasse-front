import { Tour } from "@/entities"
import { fetchAllTours } from "@/tour-service";
const dayjs = require('dayjs-with-plugins');
import { useEffect, useRef, useState } from "react"
import { Button, Row } from "react-bootstrap";

const slots = ['09:00:00', '11:00:00', '14:00:00', '16:00:00']

interface Props {
    tour: Tour;
    tours: Tour[];
    handleSlot: any;
}

export default function TourCalendar({ handleSlot, tours }: Props) {
    const [day, setDay] = useState(dayjs(new Date()).add(1, 'days'))

    function displayAvalaibleTours(oneSlot: string) {
        const slot = day.format('YYYY-MM-DD') + 'T' + oneSlot + '+00:00'
        const found = tours.find(item => slot == item.dateTime)

        if (found) {
            return <Button
                disabled
                className="button col-md-3 m-2 p-2">
                <del>
                    {dayjs(slot).utc().format('HH:mm')}
                </del>
            </Button>;
        }
        return <Button
            onClick={() => handleSlot(slot)}
            className="button col-md-3 m-2 p-2">
            {dayjs(slot).utc().format('HH:mm')}
        </Button>;
    }

    return (
        <>
            <Row className="p-2 col-md-4">

                <h3>
                    {day.isTomorrow() ?
                        <Button className='button m-2' disabled>←</Button>
                        :
                        <Button className='button m-2' onClick={() => setDay(day.subtract(1, 'days'))}>←</Button>}

                    {day.format('YYYY-MM-DD')}
                    <Button className='button m-2' onClick={() => setDay(day.add(1, 'days'))}>→</Button></h3>

                {slots.map(item =>
                    <div key={item}>{displayAvalaibleTours(item)}</div>
                )}

                <h4 className="mt-4">15 €/personne</h4>
                <p className="text-start">Conditions générales d'utilisation: Lorem ipsum dolor sit amet consectetur. Nunc lacus non erat sed leo eu praesent ornare. Sed non tincidunt elementum lacus rhoncus hac. Vitae euismod eu enim faucibus tristique ultricies. Enim morbi aenean tincidunt quis. Cras eu enim dui eu at lacus arcu tincidunt. Ut pellentesque sit et nisl sed viverra tortor gravida. Tempus condimentum cursus urna nunc massa. Ornare.</p>
            </Row>
        </>
    )
}