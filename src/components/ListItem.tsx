import { Post } from "@/entities";
import { Button } from "react-bootstrap";

interface Props {
    post: Post;
}

export default function ListItem({ post }: Props) {

    return (
        <>
            <article className="row col-sm-4 col-md-4 justify-content-center align-content-end">
                <h3 className="d-flex justify-content-center text-center p-2">{post.title}</h3>
                <img src={post.image} className="d-flex rounded-circle p-2 img-thumbnail" alt="Photographie du défunt ou du monument"></img>
                <Button className="button d-flex justify-content-center text-center col-sm-6 col-md-6 m-4 p-2" href={"/post/" + post.id}>Accéder à la fiche
                </Button>
            </article>
        </>
    )
}