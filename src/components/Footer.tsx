import { Container, Nav, Navbar } from "react-bootstrap";

export default function Footer() {

    return (
        <>
            <Navbar className="mt-4">
                <Container>
                <Nav className="justify-content-end align-items-center d-flex" style={{ width: "100%" }}>
                    <Nav.Link className="navbar p-2 pe-4" href="#infos">Conditions générales d'utilisation</Nav.Link>
                </Nav>
                </Container>
            </Navbar>
        </>
    )

}