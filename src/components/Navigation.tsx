import { Container, Nav, Navbar } from "react-bootstrap"


export default function Navigation() {

    return (
        <>
            <Navbar className="fixed-top">
                <Container>
                <Nav className="justify-content-end align-items-center d-flex" style={{ width: "100%" }}>
                    <Nav.Link className="link navbar p-2 pe-4" href="#infos">À propos</Nav.Link>
                    <Nav.Link className="link navbar p-2 pe-4" href="#glossary">Glossaire</Nav.Link>
                    <Nav.Link className="link navbar p-2 pe-4" href="#booking">Visites</Nav.Link>
                </Nav>
                </Container>
            </Navbar>
        </>
    )

}