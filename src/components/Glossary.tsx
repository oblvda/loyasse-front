import { Post } from "@/entities";
import { fetchPostsByLetter } from "@/post-service";
import { useEffect, useState } from "react";
import ListItem from "./ListItem";

export default function Glossary() {
    const [posts, setPosts] = useState<Post[]>([]);
    const [letter, setLetter] = useState("")

    useEffect(() => {
        if (letter) {
            fetchPostsByLetter(letter).then(data => {
                setPosts(data);
            });
        }
    }, [letter])

    return (
        <>
            <div className="row me-2 ms-2">
                {'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('').map(item =>
                    <h3 key={item} className="d-flex justify-content-center col-1 mb-5 align-content-center glossary" onClick={() => setLetter(item)}>{item}</h3>)}
            </div>
            <div>
                <div className="row justify-content-start text-center">
                    {
                        posts.map(item =>
                            <ListItem key={item.id} post={item} />
                        )
                    }
                </div>
            </div>
        </>
    )
}