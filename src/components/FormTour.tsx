import { Tour } from "@/entities";
import { FormEvent, useEffect, useState } from "react";
import TourCalendar from "./TourCalendar";
const dayjs = require('dayjs-with-plugins');
import { Button, Modal, Row } from "react-bootstrap";
import { fetchAllTours } from "@/tour-service";

interface Props {
    onSubmit: (tour: Tour) => void;
}

export default function FormTour({ onSubmit }: Props) {
    const [tours, setTours] = useState<Tour[]>([])

    const [errors, setErrors] = useState('');
    const [bookedTour, setBookedTour] = useState<Tour>({
        dateTime: '',
        visitorsNumber: 4,
        email: ''
    });

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function handleShowIf() {
        if (bookedTour.email && bookedTour.email && bookedTour.visitorsNumber) {
            handleShow();
            updateTours();
        }
    }

    function handleChange(event: any) {
        setBookedTour({
            ...bookedTour,
            [event.target.name]: event.target.type === 'number' ? parseInt(event.target.value) : event.target.value
        });
    }

    function handleSlot(slot: string) {
        setBookedTour({
            ...bookedTour,
            dateTime: slot
        });
    }

    useEffect(() => {
        updateTours();
    }, []);

    function updateTours() {
        fetchAllTours().then(data => {
            setTours(data);
        });
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        try {
            onSubmit(bookedTour);
            updateTours();

        } catch (error: any) {
            if (error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }

    return (
        <>
            <form onSubmit={handleSubmit}>
                {errors && <p>{errors}</p>}
                <Row className="p-2 justify-content-center">
                    <input
                        type="text"
                        name="email"
                        placeholder="Votre adresse email"
                        value={bookedTour.email}
                        onChange={handleChange}
                        required
                        className="p-2 me-4 ms-4 col-md-4 col-lg-2" />
                </Row>

                <Row className="p-2 justify-content-center">
                    <label className="p-2 col-md-4 col-lg-2 text-center">Nombre de visiteurs</label>
                    <input
                        type="number"
                        min={4}
                        max={12}
                        name="visitorsNumber"
                        value={bookedTour.visitorsNumber}
                        onChange={handleChange}
                        required
                        className="p-2 col-md-4 col-lg-1 text-start" />
                </Row>

                <Row className="p-2 justify-content-center text-center">
                    <TourCalendar
                        tour={bookedTour}
                        tours={tours}
                        handleSlot={handleSlot}
                    />
                </Row>

                <Row className="p-2 justify-content-center">
                    <Button type="submit" className="button col-md-2 p-2" onClick={handleShowIf}>Réserver</Button>
                </Row>

                <div className="modal show"
                    style={{ display: 'block', position: 'initial' }}>
                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header>
                            <Modal.Title>Réservation validée</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <p>Vous avez réservé la visite du {dayjs(bookedTour.dateTime).utc().format('YYYY-MM-DD')} à {dayjs(bookedTour.dateTime).utc().format('HH:mm')}. Merci et à bientôt!</p>
                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>Fermer</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </form>
        </>
    )
}