export interface User {
    id?:number;
    email:string;
    password?:string;
    roles:string[];
}

export interface Tour {
    id?:number;
    dateTime:Date|string;
    visitorsNumber:number;
    email:string;
}

export interface Post {
    id?:number;
    title:string;
    text:string;
    image:string;
    letter:string;
    owner:User;
}