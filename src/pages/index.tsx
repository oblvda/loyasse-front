import BannerIndex from "@/components/BannerIndex";
import Footer from "@/components/Footer";
import FormTour from "@/components/FormTour";
import Glossary from "@/components/Glossary";
import { Post, Tour } from "@/entities";
import { fetchAllPosts } from "@/post-service";
import { fetchAllTours, postTour } from "@/tour-service";
import { GetServerSideProps } from "next";
import Link from "next/link";
import { Button, Row } from "react-bootstrap";

interface Props {
    posts: Post[];
    tours: Tour[];
}

export default function Home() {

    async function addTour(tour: Tour) {
        await postTour(tour)
    }

    return (
        <main>
            <BannerIndex />
            <div className="p-4" id="infos"></div>
            <div className="container-fluid pt-2 mt-4 ps-5 pe-5">
                <Row>
                    <h2 className="text-center pb-3">Découvrir le lieu</h2>
                    <p>
                        Premier des cimetières « modernes » ouvert en 1807, le Conseil Municipal de Lyon y accorde les premières concessions à partir de 1813. Le plan initialement dessiné par Joseph Jean Pascal Gay, est radioconcentrique, car il permet de retrouver l’organisation du cimetière paroissial, le sacré ayant une place centrale. La croix magistrale fut cependant enlevée dès 1881 suite à la loi imposant la neutralité des cimetières. Lors de l’agrandissement du cimetière grâce à l’acquisition de la propriété Caille, un plan en forme de croix latine est ajouté et en son centre, un monument à la gloire des pompiers est érigé comme hymne à la laïcité. Ce cimetière, un des plus riche de France en matière de patrimoine funéraire présente une variété importante de chapelles et monuments représentatifs de l’art du 19ème siècle.
                    </p>
                    <p>
                        Des patronymes illustrent rimant avec l’histoire de Lyon sont inhumés au cimetière de Loyasse ancien. Les concessions étaient à l’origine des masses de 25 m² et sont aujourd’hui de superficies diverses et peuvent accueillir un caveau neuf ou être proposées déjà équipées, notamment lors de la vente aux enchères annuelle. Ce cimetière possède également un terrain commun pour des sépultures individuelles gratuites pour 5 ans, des jardins cinéraires et bientôt de nouveaux espaces cinéraires avec un espace de dispersion.
                    </p>
                    <div className="text-center mb-4 mt-4">
                        <h3 className="m-0">Horaires d'ouverture</h3>
                        <div>
                            <p className="m-0">Du 3 novembre au 30 avril : du lundi au dimanche de 8h à 17h</p>
                            <p className="mt-0">Du 2 mai au 2 novembre inclus : du lundi au dimanche de 8h à 17h30</p>
                        </div>
                        <h3 className="m-0">Adresse</h3>
                        <div>
                            <p className="m-0">43 rue Cardinal Gerlier</p>
                            <p className="m-0">69005</p>
                            <p className="m-0">Lyon</p>
                        </div>
                    </div>
                    <Row className="justify-content-center text-center">
                        <Button className="button col-md-2 p-2 mt-2 mb-4">
                            <Link href="#booking">Réservez votre visite</Link>
                        </Button>
                    </Row>
                    <div className="p-4" id="glossary"></div>
                </Row>
            </div >
            <div className="container-fluid">
                <h2 className="text-center p-3" id="glossary">Glossaire</h2>
                <Glossary />
                <div className="p-4" id="booking"></div>
            </div>
            <div className="container-fluid">
                <h2 className="text-center p-3" id="booking">Réserver la visite</h2>
                <FormTour onSubmit={addTour} />
            </div>
            <Footer />
        </main>
    )
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {

    return {
        props: {
            posts: await fetchAllPosts(),
            tours: await fetchAllTours()
        }
    }
}

