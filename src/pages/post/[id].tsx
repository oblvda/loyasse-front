import { Post } from "@/entities";
import { fetchOnePost } from "@/post-service";
import { GetServerSideProps } from "next";
import { Button, Row } from "react-bootstrap";

interface Props {
    post: Post
}

export default function PostPage({ post }: Props) {

    return (
        <>
            <div className="p-4" id="infos"></div>
            <div className="container-fluid justify-content-center pt-2 mt-4">
                <Row className="justify-content-center">
                    <h1 className="text-center pb-3">{post?.title}</h1>
                    <img src={post?.image} alt="Photographie du défunt ou du monument" className="pb-3 img-page rounded-circle"></img>
                </Row>
                <Row className="justify-content-center">
                    <p className="ps-5 pe-5 col-6">{post?.text}</p>
                </Row>
                <Row className="justify-content-center">
                    <Button className="button col-2 m-4 p-2" href={"/#glossary"}>Retour à la liste</Button>
                </Row>
            </div>
        </>
    )

}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    
    const { id } = context.query;

    try {

        return {
            props: {
                post: await fetchOnePost(Number(id))
            }
        }

    } catch {
        return {
            notFound: true
        }
    }
}
