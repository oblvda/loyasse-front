import axios from "axios";
import { Post } from "./entities";

export async function fetchAllPosts() {
    const response = await axios.get<Post[]>('http://localhost:8000/api/post');
    return response.data;
}

export async function fetchOnePost(id:number) {
    const response = await axios.get<Post>('http://localhost:8000/api/post/'+id);
    return response.data;
}

export async function fetchPostsByLetter(letter:string) {
    const response = await axios.get<Post[]>('http://localhost:8000/api/post/letter/'+letter);
    return response.data;
}